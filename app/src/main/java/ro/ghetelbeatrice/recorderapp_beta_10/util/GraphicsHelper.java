package ro.ghetelbeatrice.recorderapp_beta_10.util;

import android.widget.ImageView;

public class GraphicsHelper {

    /**
     * Method to simplify the process of disabling and "graying out" buttons / image views
     * @param item = the item to be disabled
     * @param alphaValue = how "grayed out" it should be
     */
    public static void disableImageButton(ImageView item, float alphaValue) {
        item.setEnabled(false);
        item.setAlpha(alphaValue);
    }

    /**
     * Method to re-enable a disabled button / image view
     * @param item = item to be re-enabled
     */
    public static void enableImageButton(ImageView item) {
        item.setEnabled(true);
        item.setAlpha(1f);
    }
}
