package ro.ghetelbeatrice.recorderapp_beta_10.util;

import java.io.File;

public class PathsHelper {

    /**
     * Method to generate custom folder in the external storage for the RecorderAPP media files
     * @param customSubfolder
     * @return
     */
    public static String getFilePath(String customSubfolder) {

        File root = android.os.Environment.getExternalStorageDirectory();
        File fileDirectories = new File(root.getAbsolutePath() + "/RecorderAPP/" + customSubfolder + "/");
        if (!fileDirectories.exists()) {
            fileDirectories.mkdirs();
        }

        return fileDirectories.getAbsolutePath();
    }
    public static String getFilePathForPhoto(String customSubfolder) {

        File root = android.os.Environment.getExternalStorageDirectory();
        File fileDirectories = new File(root.getAbsolutePath() + "/PhotoModuleAPP/" + customSubfolder + "/");
        if (!fileDirectories.exists()) {
            fileDirectories.mkdirs();
        }

        return fileDirectories.getAbsolutePath();
    }
}
