package ro.ghetelbeatrice.recorderapp_beta_10;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;

    private ImageButton btnSoundRecorder;
    private ImageButton btnVideoRecorder;
    private ImageButton btnPhotoRecord;
    private ImageButton btnEmergency;
    private Button btnSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSoundRecorder = (ImageButton) findViewById(R.id.btnGoToSoundRecorder);
        btnSoundRecorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSoundRecordActivity();
            }
        });

        btnVideoRecorder = (ImageButton) findViewById(R.id.btnGoToVideoRecorder);
        btnVideoRecorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openVideoRecordActivity();
            }
        });

        btnPhotoRecord = (ImageButton) findViewById(R.id.btnGoToPhotoRecord);
        btnPhotoRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPhotoRecordActivity();
            }
        });

        btnSettings = (Button) findViewById(R.id.btnSettings);
        btnSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSettingsActivity();
            }
        });

        btnEmergency = (ImageButton) findViewById(R.id.btnEmergency);
        btnEmergency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emergencyEmail();
            }
        });


    }

    public void openSoundRecordActivity() {
        Intent intent = new Intent(this, RecorderActivity.class);
        startActivity(intent);
    }

    public void openVideoRecordActivity() {
        Intent intent = new Intent(this, VideoRecorder.class);
        startActivity(intent);
    }

    public void openPhotoRecordActivity() {
        Intent intent = new Intent(this, PhotoRecord.class);
        startActivity(intent);
    }

    public void openSettingsActivity() {
        Intent intent = new Intent(this, EditSettings.class);
        startActivity(intent);
    }

    private void emergencyEmail() {

        String destEmail = "";
        String fromEmail = "";

        sharedPreferences = getSharedPreferences(
                EditSettings.MY_PREFERENCE,
                Context.MODE_PRIVATE);

        if (sharedPreferences.contains(EditSettings.FROM)) {
            fromEmail = (sharedPreferences.getString(EditSettings.FROM, ""));
        }
        if (sharedPreferences.contains(EditSettings.DEST_EMAIL)) {
            destEmail = (sharedPreferences.getString(EditSettings.DEST_EMAIL, ""));
        }
//        File filelocation = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), filename);
//        Uri path = Uri.fromFile(filelocation);
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("vnd.android.cursor.dir/email");
        String[] to = {destEmail};

        emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "!! EMERGENCY INFO !!");
        emailIntent.putExtra(Intent.EXTRA_TEXT, R.string.email_subject);
//        emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("content://path/to/email/attachment"));
        startActivity(Intent.createChooser(emailIntent , "Send email..."));



        Toast.makeText(MainActivity.this, "Attempt to send emergency data!", Toast.LENGTH_LONG).show();
    }
}