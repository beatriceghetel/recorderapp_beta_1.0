package ro.ghetelbeatrice.recorderapp_beta_10;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class EditSettings extends AppCompatActivity {

    SharedPreferences sharedPreferences;

    TextView name;
    TextView email;

    public static final String MY_PREFERENCE = "mypref";
    public static final String FROM = "fromKey";
    public static final String DEST_EMAIL = "destEmailKey";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_settings);
        name = (TextView) findViewById(R.id.etDestEmail);
        email = (TextView) findViewById(R.id.etFrom);
        sharedPreferences = getSharedPreferences(MY_PREFERENCE, Context.MODE_PRIVATE);
        if (sharedPreferences.contains(FROM)) {
            name.setText(sharedPreferences.getString(FROM, ""));
        }
        if (sharedPreferences.contains(DEST_EMAIL)) {
            email.setText(sharedPreferences.getString(DEST_EMAIL, ""));
        }
    }

    public void Save(View view) {
        String n = name.getText().toString();
        String e = email.getText().toString();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(FROM, n);
        editor.putString(DEST_EMAIL, e);
        editor.commit();
    }

    public void clear(View view) {
        name = (TextView) findViewById(R.id.etDestEmail);
        email = (TextView) findViewById(R.id.etFrom);
        name.setText("");
        email.setText("");

    }

    public void Get(View view) {
        name = (TextView) findViewById(R.id.etDestEmail);
        email = (TextView) findViewById(R.id.etFrom);
        sharedPreferences = getSharedPreferences(MY_PREFERENCE,
                Context.MODE_PRIVATE);

        if (sharedPreferences.contains(FROM)) {
            name.setText(sharedPreferences.getString(FROM, ""));
        }
        if (sharedPreferences.contains(DEST_EMAIL)) {
            email.setText(sharedPreferences.getString(DEST_EMAIL, ""));
        }
    }
}