package ro.ghetelbeatrice.recorderapp_beta_10;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import ro.ghetelbeatrice.recorderapp_beta_10.util.GraphicsHelper;
import ro.ghetelbeatrice.recorderapp_beta_10.util.PathsHelper;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;


public class RecorderActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int RECORD_AUDIO_REQUEST_CODE = 123;
    private static final String FILENAME_ROOT = "SOUND_REC_%s";
    private static final String DEFAULT_DATE_SUFFIX_FORMAT = "yyyyMMdd_HHmmss";

    private Chronometer chronometer;
    private MediaRecorder mediaRecorder;
    private MediaPlayer mediaPlayer;

    private ImageButton btnRecord;
    private ImageButton btnPlay;
    private ImageButton btnStop;
    private ImageButton btnStopPlay;
    private ImageButton btnGoToSpeechToText;

    private String audioSavePathInDevice = null;
    private float defaultAlphaValue = 0.5f;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recorder_activity);

        btnRecord = (ImageButton) findViewById(R.id.btnStartRecord);

        btnGoToSpeechToText = (ImageButton) findViewById(R.id.btnGoToSpeechToText);
        btnGoToSpeechToText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSpeechToTextActivity();
            }
        });

        btnStop = (ImageButton) findViewById(R.id.btnStopRecord);
        GraphicsHelper.disableImageButton(btnStop, defaultAlphaValue);

        btnPlay = (ImageButton) findViewById(R.id.btnPlayRecord);
        GraphicsHelper.disableImageButton(btnPlay, defaultAlphaValue);

        btnStopPlay = (ImageButton) findViewById(R.id.btnStopPlayRecord);
        GraphicsHelper.disableImageButton(btnStopPlay, defaultAlphaValue);

        chronometer = findViewById(R.id.chronometerTimer);

        btnRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (checkPermission()) {

                    audioSavePathInDevice =
                            (PathsHelper.getFilePath("SoundRecords") + "/" + generateFilename() + ".3gp");

                    MediaRecorderReady();

                    try {
                        mediaRecorder.prepare();
                        mediaRecorder.start();

                        chronometer.setBase(SystemClock.elapsedRealtime());
                        chronometer.start();

                    } catch (IllegalStateException | IOException e) {
                        e.printStackTrace();
                    }

                    GraphicsHelper.disableImageButton(btnRecord, defaultAlphaValue);
                    GraphicsHelper.enableImageButton(btnStop);
                    GraphicsHelper.disableImageButton(btnPlay, defaultAlphaValue);
                    GraphicsHelper.disableImageButton(btnStopPlay, defaultAlphaValue);

                    Toast.makeText(
                            RecorderActivity.this,
                            "Recording started",
                            Toast.LENGTH_LONG).show();
                } else {
                    requestPermission();
                }

            }
        });

        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mediaRecorder.stop();

                GraphicsHelper.enableImageButton(btnRecord);
                GraphicsHelper.disableImageButton(btnStop, defaultAlphaValue);
                GraphicsHelper.enableImageButton(btnPlay);

                chronometer.stop();
                int elapsedMillis = (int) (SystemClock.elapsedRealtime() - chronometer.getBase()) / 1000;
                chronometer.setBase(SystemClock.elapsedRealtime());

                Toast.makeText(
                        RecorderActivity.this,
                        "Recording Completed - " + elapsedMillis + " seconds.",
                        Toast.LENGTH_LONG).show();
            }
        });

        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) throws IllegalArgumentException,
                    SecurityException, IllegalStateException {

                btnStop.setEnabled(false);
                btnRecord.setEnabled(false);

                GraphicsHelper.enableImageButton(btnRecord);
                GraphicsHelper.disableImageButton(btnStop, defaultAlphaValue);
                GraphicsHelper.enableImageButton(btnPlay);
                GraphicsHelper.enableImageButton(btnStopPlay);

                mediaPlayer = new MediaPlayer();
                try {
                    mediaPlayer.setDataSource(audioSavePathInDevice);
                    mediaPlayer.prepare();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                mediaPlayer.start();
                Toast.makeText(
                        RecorderActivity.this,
                        "Recording Playing",
                        Toast.LENGTH_LONG).show();
            }
        });

        btnStopPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                GraphicsHelper.disableImageButton(btnStop, defaultAlphaValue);
                GraphicsHelper.enableImageButton(btnRecord);
                GraphicsHelper.disableImageButton(btnPlay, defaultAlphaValue);
                GraphicsHelper.disableImageButton(btnStopPlay, defaultAlphaValue);

                if(mediaPlayer != null){
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    MediaRecorderReady();
                }
            }
        });
    }

    private void MediaRecorderReady() {
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mediaRecorder.setOutputFile(audioSavePathInDevice);
    }

    /**
     * Helper method to generate hopefully unique files with the help of the TimeStamps
     * nothing needed as parameters.
     * @return the formatted FILE_NAME_ROOT with the suffix of the current time stamp
     */
    private String generateFilename() {
        String timeStamp = new SimpleDateFormat(DEFAULT_DATE_SUFFIX_FORMAT).format(Calendar.getInstance().getTime());
        return String.format(FILENAME_ROOT, timeStamp);
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(RecorderActivity.this, new
                String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO}, RECORD_AUDIO_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode,
            String permissions[],
            int[] grantResults) {
        switch (requestCode) {
            case RECORD_AUDIO_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean StoragePermission = grantResults[0] ==
                            PackageManager.PERMISSION_GRANTED;
                    boolean RecordPermission = grantResults[1] ==
                            PackageManager.PERMISSION_GRANTED;

                    if (StoragePermission && RecordPermission) {
                        Toast.makeText(RecorderActivity.this, "Permission Granted",
                                Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(RecorderActivity.this, "Permission Denied", Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(),
                WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(),
                RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED &&
                result1 == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onClick(View v) {

    }

    public void openSpeechToTextActivity() {
        Intent intent = new Intent(this, SpeechToTextActivity.class);
        startActivity(intent);
    }
}