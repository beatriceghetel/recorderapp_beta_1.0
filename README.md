# README #

This Readme will briefly explain the functionality of the project, and configuration steps

### What is this repository for? ###

Creation of a Recorder Android app with multiple functionalities:

* camera photo shoot
* camera video recording
* voice recording

### How do I get set up? ###

* Install Android Studio
* Install Git
* Run the **git clone https://beatriceghetel@bitbucket.org/beatriceghetel/recorderapp_beta_1.0.git** command in the folder where you want to copy the repository. Then open the Project within Android Studio.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact